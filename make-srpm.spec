#  make-srpm: srpm builder, helps build srpm out of rcs sources

#  Copyright (C) 2009-2010 Sam Liddicott <sam@liddicott.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


#makesrpm-RELEASE: HEAD
#makesrpm-ORIGIN_PATTERN: v*
#makesrpm-VERSION_PATTERN: v*
#makesrpm-PATCH_LEVEL: 1

Name: make-srpm-vcs
Source: %makesrpm_tarname
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
URL: http://repo.or.cz/w/make-srpm.git
License: GPL3 or later
Summary: make-srpm build and export tool for GIT and SVN
Group: Development/Utilities
Version: 
Release: %{release}-%{?dist}
Epoch: 0

%description
Helps production of RPM's from GIT or SVN source trees.
Useful for rebase trees of local changes against an official release.

A pristine source tar.gz is produced, with a series of patches up to the current released

%prep
%setup -q -n %makesrpm_tarprefix

%build

%install

rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/

mkdir -p $RPM_BUILD_ROOT/%{_bindir}
mkdir -p $RPM_BUILD_ROOT/%{_libdir}/make-srpm

install -m 0755 -t $RPM_BUILD_ROOT/%{_bindir} make-srpm
install -m 0755 -t $RPM_BUILD_ROOT/%{_libdir}/make-srpm make-srpm_*

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/
%{_libdir}/

%changelog
* Wed Jun 24 2009 Sam Liddicott <sam@liddicott.com>
- Dog-food build. (First spec file for make-srpm)
